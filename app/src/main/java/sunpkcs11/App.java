/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package sunpkcs11;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.PasswordCallback;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.AuthProvider;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;

public class App {

    public static void main(String[] args) throws Exception {
        Thread.setDefaultUncaughtExceptionHandler(new UEH());

        AuthProvider pkcs11Provider = (AuthProvider) Security.getProvider("SunPKCS11");
        pkcs11Provider = (AuthProvider) pkcs11Provider.configure("/home/opc/sunpkcs11/pkcs11.cfg");
        pkcs11Provider.setCallbackHandler(callbacks -> {
            for (Callback cb : callbacks) {
                if (cb instanceof PasswordCallback pcb) {
                    pcb.setPassword("riocardmais1".toCharArray());
                }
            }
        });
        Security.addProvider(pkcs11Provider);

        KeyStore ks = KeyStore.getInstance("PKCS11", pkcs11Provider);
        ks.load(null, null);
    }
}

class UEH implements Thread.UncaughtExceptionHandler {

    private static final String PKCS11_LOG_FILE = "/tmp/pkcs11.log";

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        recursiveUncaughtException(t, e);
        if (Files.exists(Path.of(PKCS11_LOG_FILE))) {
            System.err.printf("\nCheck the file '%s' for more debug info.\n", PKCS11_LOG_FILE);
        }
    }

    private void recursiveUncaughtException(Thread t, Throwable e) {
        StackTraceElement[] ses = e.getStackTrace();
        System.err.println("Exception in thread \"" + t.getName() + "\" " + e);
        for (StackTraceElement se : ses) {
            System.err.println("\tat " + se);
        }
        Throwable ec = e.getCause();
        if (ec != null) {
            recursiveUncaughtException(t, ec);
        }
    }
}
