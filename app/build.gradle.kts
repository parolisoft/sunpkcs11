plugins {
    application
}

repositories {
    mavenCentral()
}

dependencies {
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

application {
    mainClass.set("sunpkcs11.App")
}

tasks.withType<JavaExec> {
    environment["hsm_pkcs_conf"] = "$rootDir/hsm_pkcs_conf.json"
}